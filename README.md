# Tutorial API REST com Node utilizando express
Este tutorial esta dispónivel em [imasters](https://imasters.com.br/back-end/api-http-rest-conceito-e-exemplo-em-node-js)

## Resposta as perguntas do exercício
1.1) passo-a-passo do tutorial Logo abaixo  

1.2) A API é de de tempo real? Justifique.  
A api não é de tempo real, não implementa websocket, ela necessita de requisição para fornecer uma resposta.  

1.3) A API está documentada? De que forma?  
A API não está documentada. 

## Passo a Passo

1. Inicializando o projeto com package.json e as depêndencias iniciais
`npm init`

2. Atualizando package.json dependências com `npm install`
```
"dependencies": {
   "body-parser": "^1.18.3",
   "express": "^4.16.4",
   "fs": "0.0.1-security"
   ```
3. Criando arquivo server.js
```
var express = require(‘express’);
var bodyParser = require(‘body-parser’);
var fs = require(‘fs’);

var app = express();

app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

app.use(function(req, res, next){
 res.setHeader(“Access-Control-Allow-Origin”, “*”);
 res.setHeader(“Access-Control-Allow-Methods”, “GET, POST, PUT, DELETE”);
 res.setHeader(“Access-Control-Allow-Headers”, “content-type”);
 res.setHeader(“Content-Type”, “application/json”);
 res.setHeader(“Access-Control-Allow-Credentials”, true);
 next();
});

app.listen(9090, function(){ console.log(‘Servidor Web rodando na porta 9090’) });
```
3. Testar o server `npm start`

4. Criando arquivo usuarios.json de base de dados
```
{  
  “usuarios”:[  
     { 
        “nome”:”Primeiro usuário”,
        “site”:”www.primeirousuario.com.br”,
        “usuario_id”:1
     },

     { 
        “nome”:”Segundo usuário”,
        “site”:”www.segundousuario.com.br”,
        “usuario_id”:2
     },
     { 
        “nome”:”Terceiro usuário”,
        “site”:”www.terceirousuario.com.br”,
        “usuario_id”:3
     }
  ]
}
```
5. Adicionando método GET no arquivo server.js
```
app.get('/api', function(req, res){
    fs.readFile('usuarios.json', 'utf8', function(err, data){
      if (err) {
        var response = {status: 'falha', resultado: err};
        res.json(response);
      } else {
        var obj = JSON.parse(data);
        var result = 'Nenhum usuário foi encontrado';
    
        obj.usuarios.forEach(function(usuario) {
          if (usuario != null) {
            if (usuario.usuario_id == req.query.usuario_id) {
              result = usuario;
            }
          }
        });
    
        var response = {status: 'sucesso', resultado: result};
        res.json(response);
      }
    });
   });
   ```
6. Criando método POST no arquivo server.js
```
app.post('/api', function(req, res){
    fs.readFile('usuarios.json', 'utf8', function(err, data){
      if (err) {
        var response = {status: 'falha', resultado: err};
        res.json(response);
      } else {
        var obj = JSON.parse(data);
        req.body.usuario_id = obj.usuarios.length + 1;
    
        obj.usuarios.push(req.body);
    
        fs.writeFile('usuarios.json', JSON.stringify(obj), function(err) {
          if (err) {
            var response = {status: 'falha', resultado: err};
            res.json(response);
          } else {
            var response = {status: 'sucesso', resultado: 'Registro incluso com sucesso'};
            res.json(response);
          }
        });
      }
    });
   });
   ```
7. Criando método PUT no arquivo server.js
```
   app.put('/api', function(req, res){
    fs.readFile('usuarios.json', 'utf8', function(err, data){
      if (err) {
        var response = {status: 'falha', resultado: err};
        res.json(response);
      } else {
        var obj = JSON.parse(data);
    
        obj.usuarios[(req.body.usuario_id - 1)].nome = req.body.nome;
        obj.usuarios[(req.body.usuario_id - 1)].site = req.body.site;
    
        fs.writeFile('usuarios.json', JSON.stringify(obj), function(err) {
          if (err) {
            var response = {status: 'falha', resultado: err};
            res.json(response);
          } else {
            var response = {status: 'sucesso', resultado: 'Registro editado com sucesso'};
            res.json(response);
          }
        });
      }
    });
   });
```
8. Criando método DELETE no arquivo server.js
```
   app.delete('/api', function(req, res){
 fs.readFile('usuarios.json', 'utf8', function(err, data){
   if (err) {
     var response = {status: 'falha', resultado: err};
     res.json(response);
   } else {
     var obj = JSON.parse(data);
 
     delete obj.usuarios[(req.body.usuario_id - 1)];
 
     fs.writeFile('usuarios.json', JSON.stringify(obj), function(err) {
       if (err) {
         var response = {status: 'falha', resultado: err};
         res.json(response);
       } else {
         var response = {status: 'sucesso', resultado: 'Registro excluído com sucesso'};
         res.json(response);
       }
     });
   }
 });
});
```
9. Testes

- GET 
![GET](imagem_test_postman/GET.png)  
- POST
![POST](imagem_test_postman/POST.png)  
- PUT 
![PUT](imagem_test_postman/PUT.png)  
- DELETE 
![DELETE](imagem_test_postman/DELETE.png)  